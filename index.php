<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Andres Flores - Assignment 1</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="./style.css">
</head>

<body>

  <h1 class="header1">Assignment 1 - Flores, Andres</h1>

  <div id="summary">

    <h2>Summary</h2>


    <p>I am a student at BGSU studying computer science. I am learning about databases, programming languages, and other
      fun stuff. In my free time I like to exercise, cook, and make coffee</p>

    <h3>Interesting facts about myself</h3>
    <ul>

      <li>I am a first generation hispanic student.</li>
      <li>I like biology</li>
      <li>I like designing things</li>
    </ul>
  </div>

  <div id="personalInfo">
    <h2>Personal Information</h2>

    <div class="personalinfocontentwrapper">
      <div class="personalinfoinner">
        <p class="personalinfop1">Name:</p>
        <p class="personalinfop2">Andres Flores</p>

      </div>
      <div class="personalinfoinner">
        <p class="personalinfop1">Address:</p>
        <p class="personalinfop2">Painesville, OH</p>
      </div>
      <div class="personalinfoinner">
        <p class="personalinfop1">Phone Number:</p>
        <p class="personalinfop2">4401234567</p>
      </div>
      <div class="personalinfoinner">
        <p class="personalinfop1">Email:</p>
        <p class="personalinfop2">andres@gmail.com</p>
      </div>

    </div>
  </div>


  <div id="academicInfo">
    <h2>Academic Information</h2>

    <ul>
      <li>
        <p>
          <strong>College: </strong> Bowling Green State University (3.8 GPA)
        </p>
      </li>
      <li>
        <p>
          <strong>High School: </strong> T.W. Harvey High School (3.4 GPA)
        </p>
      </li>
      <li>
        <p><strong>Notable Coursework: </strong>E-CITY, Data Structures, Web Application Development</p>
      </li>

    </ul>




  </div>
  <div id="employmentInfo">
    <h2>Employment Information</h2>
    <ul>
      <li>
        <p><strong>Software Engineer</strong> - Apple</p>
      </li>
      <li>
        <p><strong>Financial Advisor</strong> - JP Morgan Chase</p>
      </li>
      <li>
        <p><strong>Warehouse Associate</strong> - Heilind Electronics</p>
      </li>
      <li>
        <p><strong>Attendant</strong> - Laundromat</p>
      </li>
    </ul>




  </div>


</body>

</html>